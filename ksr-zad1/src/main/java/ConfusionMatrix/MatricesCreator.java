package ConfusionMatrix;

import Divisions.Category;

import java.util.ArrayList;
import java.util.List;

public class MatricesCreator {

    private List<ProbablititySet> results;
    private List<ProbabilityMatrix> matrices;

    public MatricesCreator(List<ProbablititySet> results, List<Category> categories) {
        this.results = results;
        matrices = new ArrayList<>();
        measure(categories);
    }

    private void measure(List<Category> categories) {
        createCategories(categories);
        for (ProbabilityMatrix probabilityMatrix : matrices) {
            fulfillMatrix(probabilityMatrix);
        }
        for (ProbabilityMatrix p : matrices) {
            System.out.println(p);
        }
    }

    private void fulfillMatrix(ProbabilityMatrix probabilityMatrix) {
        // po wszystkich danych z różnych artykułów
        for (ProbablititySet probabilitySet : results) {
            for (String predicted : probabilitySet.predicted) {
                // po narzuconych etykietach
                addValueInField(probabilityMatrix, predicted, probabilitySet.assesment);
            }
        }
    }


    private void addValueInField(ProbabilityMatrix probabilityMatrix, String predicted, String result) {
        if (probabilityMatrix.label.equals(result)) {
            if (predicted.contains(result)) {
                probabilityMatrix.TP += 1;
            } else {
                probabilityMatrix.FP += 1;
            }
        } else {
            if (predicted.contains(probabilityMatrix.label)) {
                probabilityMatrix.FN += 1;
            } else {
                probabilityMatrix.TN += 1;
            }
        }
    }


    private void changeFP(ProbabilityMatrix probabilityMatrix) {
        probabilityMatrix.FP += 1;
        for (ProbabilityMatrix pm : matrices) {
            if (pm.equals(probabilityMatrix)) {
                pm.FN += 1;
            }
        }
    }

    private void changeTP(ProbabilityMatrix probabilityMatrix) {
        probabilityMatrix.TP += 1;
        for (ProbabilityMatrix pm : matrices) {
            if (!pm.equals(probabilityMatrix)) {
                pm.TN += 1;
            }
        }
    }

    private void createCategories(List<Category> categories) {
        for (Category category : categories) {
            ProbabilityMatrix probabilityMatrix = new ProbabilityMatrix(category.label);
            matrices.add(probabilityMatrix);
        }
    }
}
