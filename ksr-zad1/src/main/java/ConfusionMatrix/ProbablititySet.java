package ConfusionMatrix;

import java.util.List;

public class ProbablititySet {

    public List<String> predicted;
    public String assesment;

    public ProbablititySet(List<String> predicted, String assesment) {
        this.predicted = predicted;
        this.assesment = assesment;
    }

    @Override
    public String toString() {
        return "ProbablititySet{" +
                "predicted=" + predicted +
                ", result=" + assesment +
                "}\n";
    }
}
