package ConfusionMatrix;

public class ProbabilityMatrix {

    /****************************************************
     *  kategoria: polska     |   kategoria: polska     *
     *  wyszło:    polska     |   wyszło: nie polska    *
     *  TP                    |   FP                    *
     * -----------------------+------------------------ *
     *  kategoria: nie polska |   kategoria: nie polska *
     *  wyszło:    polska     |   wyszło : nie polska   *
     *  FN                    |   TN                    *
     ****************************************************/

    public int TP;
    public int FP;
    public int FN;
    public int TN;
    public String label;

    public ProbabilityMatrix(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return  "-----------------\n" +
                " "  + label + "\n" +
                "-----------------\n" +
                "  TP: " + TP + "|  FP: " + FP  +   "\n" +
                " -------+------- \n" +
                "  FN: " + FN + "|  TN: " + TN  +   "\n" +
                "----------------\n";
    }
}
