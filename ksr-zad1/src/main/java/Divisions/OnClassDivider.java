package Divisions;

import VectorSpaceModel.VectorFeature;
import java.util.ArrayList;
import java.util.List;

public class OnClassDivider {

    public List<Category> categories;
    public List<String> labels;

    public OnClassDivider(List<VectorFeature> toLearn, String labelsName) {
        this.categories = new ArrayList<>();
        this.labels = new ArrayList<>();

        separateSets(toLearn);
    }

    private void separateSets(List<VectorFeature> toLearn) {
        getLabels(toLearn);
        divideOnClasses(toLearn);
    }

    /***
     * Zbieramy wszystkie etykiety, które występują we wszystkich artykułach.
     * @param toLearn - lista wszystkich artykułów
     */
    private void getLabels(List<VectorFeature> toLearn) {
        for (VectorFeature vf : toLearn) {
            for (String place : vf.article.getPlaces()) {
                if (!labels.contains(place)) {
                    labels.add(place);
                }
            }
        }
    }

    /**
     * Dodaje nową kategorię i zleca funkcji lookforArticleOfThisLabel znalezienie wszystkich artykułów należących do
     * tej kategorii.
     * @param toLearn
     */
    private void divideOnClasses(List<VectorFeature> toLearn) {
        for (String label : labels) {
            Category category = new Category(label);
            categories.add(category);
            lookForArticleOfThisLabel(toLearn, label);
        }
    }

    /**
     * Przegląda etykiety wszystkich artykułów i jeśli etykieta należy do danej kategorii to jest dodawana do listy.
     * @param toLearn
     * @param label
     */
    private void lookForArticleOfThisLabel(List<VectorFeature> toLearn, String label) {
        for (VectorFeature vf : toLearn) {
            for (String place : vf.article.getPlaces()) {
                if (label.equals(place)) {
                    categories.get(indexOfLabel(place)).vector.add(vf.features);
                }
            }
        }
    }

    private int indexOfLabel(String place) {
        for (int iter = 0; iter < labels.size(); iter++) {
            if (labels.get(iter).equals(place)) {
                return iter;
            }
        }
        return (-1);
    }
}
