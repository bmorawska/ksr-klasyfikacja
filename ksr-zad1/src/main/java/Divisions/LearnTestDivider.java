package Divisions;

import DataSet.routers.Article;

import java.util.ArrayList;
import java.util.List;

public class LearnTestDivider {

    public List<Article> testSetArticles;
    public List<Article> learningSetArticles;
    private List<Article> articles;

    private int testSet;

    public LearnTestDivider(List<Article> articles, int testSetPercent) {

        this.testSet = (int) ((double) articles.size() * ((double) testSetPercent / 100));
        this.learningSetArticles = new ArrayList<>();
        this.articles = articles;
        separateSets();
    }

    private void separateSets() {
        if (testSet != 0) {
            testSetArticles = articles.subList(0, testSet);
        } else {
            testSetArticles = new ArrayList<>();
        }
        if (testSet != articles.size()) {
            learningSetArticles = articles.subList(testSet + 1, articles.size());
        } else {
            learningSetArticles = new ArrayList<>();
        }

        System.out.println("Test size: " + testSetArticles.size());
        System.out.println("------------------------------------");
        System.out.println("Learn size: " + learningSetArticles.size());
    }
}
