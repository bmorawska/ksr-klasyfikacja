package Divisions;

import java.util.ArrayList;
import java.util.List;

public class Category {

    public List<double[]> vector;
    public String label;

    public Category(String label) {
        vector = new ArrayList<>();
        this.label = label;
    }

    private String showVector() {
        StringBuilder stringBuilder = new StringBuilder();
        for (double[] tab : vector) {
            for (int i = 0; i < tab.length; i++) {
                stringBuilder.append(tab[i]);
                stringBuilder.append(", ");
            }
            stringBuilder.append("\n");
        }
        return (stringBuilder.toString());
    }

    @Override
    public String toString() {
        return "------------------" + label + "--------------------\n" + showVector();
    }
}
