package DataSet.routers;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Article {

    @SerializedName("attrs")
    @Expose
    private Attrs attrs;
    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("companies")
    @Expose
    private List<Object> companies = null;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("dateline")
    @Expose
    private String dateline;
    @SerializedName("exchanges")
    @Expose
    private List<Object> exchanges = null;
    @SerializedName("orgs")
    @Expose
    private List<Object> orgs = null;
    @SerializedName("places")
    @Expose
    private List<String> places = null;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("topics")
    @Expose
    private List<String> topics = null;
    @SerializedName("unknown")
    @Expose
    private String unknown;

    private List<String> bodyWords = new ArrayList<>();

    public List<String> getBodyWords() {
        return bodyWords;
    }

    public void setBodyWords(List<String> bodyWords) {
        this.bodyWords = bodyWords;
    }

    public Attrs getAttrs() {
        return attrs;
    }

    public void setAttrs(Attrs attrs) {
        this.attrs = attrs;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public List<Object> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Object> companies) {
        this.companies = companies;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateline() {
        return dateline;
    }

    public void setDateline(String dateline) {
        this.dateline = dateline;
    }

    public List<Object> getExchanges() {
        return exchanges;
    }

    public void setExchanges(List<Object> exchanges) {
        this.exchanges = exchanges;
    }

    public List<Object> getOrgs() {
        return orgs;
    }

    public void setOrgs(List<Object> orgs) {
        this.orgs = orgs;
    }

    public List<String> getPlaces() {
        return places;
    }

    public void setPlaces(List<String> places) {
        this.places = places;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getTopics() {
        return topics;
    }

    public void setTopics(List<String> topics) {
        this.topics = topics;
    }

    public String getUnknown() {
        return unknown;
    }

    public void setUnknown(String unknown) {
        this.unknown = unknown;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("attrs", attrs).append("body", body).append("companies", companies).append("date", date).append("dateline", dateline).append("exchanges", exchanges).append("orgs", orgs).append("places", places).append("title", title).append("topics", topics).append("unknown", unknown).toString();
    }

}