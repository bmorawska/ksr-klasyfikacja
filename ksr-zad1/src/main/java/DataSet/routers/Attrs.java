package DataSet.routers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Attrs {

    @SerializedName("cgisplit")
    @Expose
    private String cgisplit;
    @SerializedName("lewissplit")
    @Expose
    private String lewissplit;
    @SerializedName("newid")
    @Expose
    private String newid;
    @SerializedName("oldid")
    @Expose
    private String oldid;
    @SerializedName("topics")
    @Expose
    private String topics;

    public String getCgisplit() {
        return cgisplit;
    }

    public void setCgisplit(String cgisplit) {
        this.cgisplit = cgisplit;
    }

    public String getLewissplit() {
        return lewissplit;
    }

    public void setLewissplit(String lewissplit) {
        this.lewissplit = lewissplit;
    }

    public String getNewid() {
        return newid;
    }

    public void setNewid(String newid) {
        this.newid = newid;
    }

    public String getOldid() {
        return oldid;
    }

    public void setOldid(String oldid) {
        this.oldid = oldid;
    }

    public String getTopics() {
        return topics;
    }

    public void setTopics(String topics) {
        this.topics = topics;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("cgisplit", cgisplit).append("lewissplit", lewissplit).append("newid", newid).append("oldid", oldid).append("topics", topics).toString();
    }

}