package DataSet.routers;

import DataSet.routers.Article;
import DataSet.routers.FileArticles;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * Klasa wczytuje wszystkie dane podane w formacie JSON, ze wszystkich plików, które są w danym poprzez konstruktor
 * katalogu.
 */

public class ReadData {

    public List<Article> articles = new ArrayList<>();
    private static int uniqueCounter = 0;
    private String directoryName;


    public ReadData(String directoryName) {

        this.directoryName = directoryName;
    }

    /***
     * Funkcja jest odpowiedzialna za nadzór nad wczytywaniem plików z podanego katalogu.
     * Dla każego pliku w katalogu wywołuje funkcje, która wczyta z tego pliku artykuły.
     */
    public void readDirectory() {
        File folder = new File(directoryName);
        File[] fileNames = folder.listFiles();
        assert fileNames != null;
        for (File file : fileNames) {
            //System.out.println(file.getName());
            if (file.isFile()) {
                //System.out.println(file.getName());
                readFile(file.getAbsolutePath(), file.getName());
            }
            uniqueCounter = 0;
        }
    }

    /***
     * Funkcja wczytuje jeden z plików JSON z podanego katalogu, pobiera z niego wszystkie
     * artykuły i dodaje je do listy artykułów z unikalną nazwą (nazwa pliku + numer).
     * @param filename - nazwa pliku z artykułami
     */
    private void readFile(String PATH, String filename) {

        Gson gson = new Gson();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(PATH));
            FileArticles fileArticles = gson.fromJson(bufferedReader, FileArticles.class);

            if (fileArticles != null) {
                for (Article article : fileArticles.getArticles()) {
                    articles.add(article);
                }
            }
        } catch (
                FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}