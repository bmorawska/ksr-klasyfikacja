package DataSet.wordTransformations;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class ReadWordsFromFile {

    private final static String irregulaVerbsFilename = "words-transformations/irregularVerbs.json";

    /***
     * Wczytuje listę czasowników nieregularnych z pliku words-transformations/irregularVerbs.json.
     * @return zwraca listę czasowników nieregularnych
     */
    public List<List<String>> readIrregularVerbs() {
        List<List<String>>  irregularVerbsList = new ArrayList<>();
        Gson gson = new Gson();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(irregulaVerbsFilename));
            IrregularVerbs irregularVerbs = gson.fromJson(bufferedReader, IrregularVerbs.class);
            irregularVerbsList = irregularVerbs.getIrregularVerbs();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return irregularVerbsList;
    }
}
