package DataSet.wordTransformations;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IrregularVerbs {

    @SerializedName("irregularVerbs")
    @Expose
    private List<List<String>> irregularVerbs = null;

    public List<List<String>> getIrregularVerbs() {
        return irregularVerbs;
    }

    public void setIrregularVerbs(List<List<String>> irregularVerbs) {
        this.irregularVerbs = irregularVerbs;
    }

}