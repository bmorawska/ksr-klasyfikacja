package PreprocessingData;

import DataSet.routers.Article;
import DataSet.wordTransformations.ReadWordsFromFile;
import Statistics.Statistics;

import java.util.ArrayList;
import java.util.List;

public class Tokenizer {

    public List<Article> articles;
    private List<List<String>> irregularVerbs;

    public Tokenizer(List<Article> articles) {
        this.articles = articles;
        ReadWordsFromFile readWordsFromFile = new ReadWordsFromFile();
        irregularVerbs = readWordsFromFile.readIrregularVerbs();
    }

    /***
     * Funkcja bierze każdy artykuł z listy i każde słowo z body zamienia na listę słów.
     * W ramach tej operacji usuwane są wszystkie znaki, które nie są literami, wszystkie wyrazy
     * zostały sprowadzone do małych liter, zostały usunięte słowa krótsze niż dwuznakowe.
     *
     * Opcjonalnie wyświetla tekst (body) i zawartość tablicy dla porównania.
     */
    public void tokenize() {
        for (Article article : articles) {
            List<String> words = new ArrayList<>();
            int numberOfDeletedWords = words.size();
            processArticle(words, article);
            article.setBodyWords(words);
            numberOfDeletedWords -= article.getBodyWords().size();
            if (numberOfDeletedWords > Statistics.HIGHEST_NUMBER_OF_DELETED_WORDS) {
                Statistics.HIGHEST_NUMBER_OF_DELETED_WORDS = numberOfDeletedWords;
            }
            if (article.getBodyWords().size() > Statistics.BIGGEST_ARTICLE) {
                Statistics.BIGGEST_ARTICLE = article.getBodyWords().size();
            }
        }

        System.out.println("Statistics.HIGHEST_NUMBER_OF_DELETED_WORDS : " + Statistics.HIGHEST_NUMBER_OF_DELETED_WORDS);
        System.out.println("Statistics.BIGGEST_ARTICLE : " + Statistics.BIGGEST_ARTICLE);
    }

    /***
     * Dla tekstu (body) artykułu przeprowadza szerego operacji takich jak:
     * 1.) Wyselekcjonowanie z tekstu tylko liter alfabetu łacińskiego,
     * 2.) Zamianę wszystkich wielkich liter na małe,
     * 3.) Zamianę czasowników nieregularnych na ich formę w bezokoliczniku,
     * 4.) Stemizację
     * 5.) Walidację
     * @param words - tekst (body) zamieniony na listę słów
     * @param article - artykuł z listy artykułów
     */
    private void processArticle(List<String> words, Article article) {
        article.setBody(article.getBody().concat(article.getTitle() + " " + article.getDateline() + " "));
       //article.setBody(article.getBody().concat(" "));
        StringBuilder sb = new StringBuilder();
        //Dla każdej litery w tekście (body)...
        for (int iter = 0; iter < article.getBody().length(); iter++) {
            //Sprawdzamy czy znak jest literą...
            if (isAlphabetLetter(article.getBody().charAt(iter))) {
                //Jeśli tak to jest to część słowa, więc dodajemy do ciągu znaków.
                sb.append(article.getBody().charAt(iter));
            } else {
                /*Jeśli nie to znaczy, że skończył się wyraz, więc trzeba go zapisać, ale wcześniej sprawdzić,
                czy spełnia założone warunki */
                String word = sb.toString().toLowerCase();
                word = changeToInfinitive(word);
                word = changeCapitalToCountry(word);
                word = stemmize(word);
                if (validateNewWordSuccessfull(word)) {
                    words.add(word);
                }
                sb.setLength(0);
            }
        }
    }

    /**
     * Sprawdza, czy słowo jest dłuższe niż jeden znak.
     * @param word - słowo do sprawdzenia.
     * @return - prawda, jeśli walidacja zakończyła się sukcesem, fałsz jeśli nie
     */
    private boolean validateNewWordSuccessfull(String word) {
        if (!longerThanTwoChars(word)) {
            return (false);
        } else return !isCommonWord(word);
    }

    /**
     * Stemizacja za pomocą biblioteki Porter Stemmer
     * @param word - słowo do stemizacji
     * @return słowo po stemizacji
     */
    private String stemmize(String word) {
        Stemmer stemmer = new Stemmer();
        stemmer.add(word.toCharArray(), word.length());
        stemmer.stem();
        return (stemmer.toString());
    }

    /**
     * Sprawdza, czy słowo jest jednym ze 100 najpopularniejszych słów w języku angielskim.
     * @param word - słowo do sprawdzenia.
     * @return prawda jeśli jest, fałsz jeśli nie
     */
    private boolean isCommonWord(String word) {
        String[] commonWords = {
                "the", "be", "to", "of", "and", "in", "that", "have",
                "it", "for", "not", "with", "he", "as", "you", "do", "at", "this", "but",
                "his", "by", "from", "they", "we", "say", "her", "she", "or", "an", "will",
                "my", "one", "all", "would", "there", "their", "what", "so", "up", "out", "if",
                "about", "who", "get", "which", "go", "me", "when", "make", "can", "like", "time",
                "no", "just", "him", "know", "take", "people", "into", "year", "your", "good",
                "could", "them", "see", "other", "than", "then", "now", "look", "only", "come",
                "its", "some", "over", "think", "also", "back", "after", "use", "two", "how",
                "our", "work", "first", "well", "way", "even", "new", "want", "because", "any",
                "these", "give", "day", "most", "us", "is"
        };
        for (String commonWord : commonWords) {
            if (commonWord.equals(word)) {
                return (true);
            }
        }
        return (false);
    }

    /**
     * Lista czasowników nieregularnych wygląda tak:
     * bezokolicznik, [inne formy], [inne fromy]......
     * <p>
     * Jeśli na liście znajdzie się słowo, które jest w innej formie niż bezokolicznik, to zostanie
     * zastąpione bezokolicznikiem czasownika.
     *
     * @param word - słowo do sprawdzenia
     */
    private String changeToInfinitive(String word) {
        for (List<String> verbs : irregularVerbs) {
            for (int verbNumber = 1; verbNumber < verbs.size(); verbNumber++) {
                if (verbs.get(verbNumber).equals(word)) {
                    word = verbs.get(0);
                    return word;
                }
            }
        }
        return word;
    }

    /**
     * Sprawdza, czy wyraz jest dłuższy niż 2 znaki. Krótsze nie nadają się do dalszej obróbki.
     * @param word - słowo do sprawdzenia.
     * @return prawda jeślo jest, fałsz jeśli nie
     */
    private boolean longerThanTwoChars(String word) {
        return (word.length() > 2);
    }

    /**
     * Sprawdza, czy znak jest literą alfabetu łacińskiego.
     * @param letter - litera do sprawdzenia
     * @return - prawda jeśli jest, fałsz jeśli nie
     */
    private boolean isAlphabetLetter(char letter) {
        return ((letter >= 'a' && letter <= 'z') || (letter >= 'A' && letter <= 'Z'));
    }

    private String changeCapitalToCountry(String word) {
        final String[][] tab = {
                {"afghanistan", "kabul"},
                {"albania", "tirana"},
                {"algeria", "algiers"},
                {"andorra", "andorra"},
                {"angola", "luanda"},
                {"argentina", "buenos"},
                {"armenia", "yerevan"},
                {"australia", "canberra"},
                {"austria", "vienna"},
                {"azerbaijan", "baku"},
                {"bahamas", "nassau"},
                {"bahrain", "manama"},
                {"bangladesh", "dhaka"},
                {"barbados", "bridgetown"},
                {"belarus", "minsk"},
                {"belgium", "brussels"},
                {"belize", "belmopan"},
                {"benin", "porto"},
                {"bhutan", "thimphu"},
                {"bolivia", "paz"},
                {"bosnia", "sarajevo"},
                {"botswana", "gaborone"},
                {"brazil", "brasilia"},
                {"bulgaria", "sofia"},
                {"burundi", "bujumbura"},
                {"cambodia", "phnom"},
                {"cameroon", "yaounde"},
                {"canada", "ottawa"},
                {"cape", "verde"},
                {"chad", "djamena"},
                {"chile", "santiago"},
                {"china", "beijing"},
                {"colombia", "bogota"},
                {"comoros", "moroni"},
                {"croatia", "zagreb"},
                {"cuba", "havana"},
                {"cyprus", "nicosia"},
                {"czech", "prague"},
                {"denmark", "copenhagen"},
                {"djibouti", "djibouti"},
                {"dominica", "roseau"},
                {"ecuador", "quito"},
                {"egypt", "cairo"},
                {"england", "london"},
                {"estonia", "tallinn"},
                {"fiji", "suva"},
                {"finland", "helsinki"},
                {"france", "paris"},
                {"gabon", "libreville"},
                {"gambia", "banjul"},
                {"georgia", "tbilisi"},
                {"germany", "berlin"},
                {"ghana", "accra"},
                {"greece", "athens"},
                {"guinea", "conakry"},
                {"guyana", "georgetown"},
                {"honduras", "tegucigalpa"},
                {"hungary", "budapest"},
                {"iceland", "reykjavik"},
                {"india", "delhi"},
                {"indonesia", "jakarta"},
                {"iran", "tehran"},
                {"iraq", "baghdad"},
                {"ireland", "dublin"},
                {"israel", "aviv"},
                {"italy", "rome"},
                {"jamaica", "kingston"},
                {"japan", "tokyo"},
                {"jordan", "amman"},
                {"kazakhstan", "astana"},
                {"kenya", "nairobi"},
                {"kosovo", "pristina"},
                {"kuwait", "kuwait"},
                {"kyrgyzstan", "bishkek"},
                {"laos", "vientiane"},
                {"latvia", "riga"},
                {"lebanon", "beirut"},
                {"lesotho", "maseru"},
                {"liberia", "monrovia"},
                {"libya", "tripoli"},
                {"liechtenstein", "vaduz"},
                {"lithuania", "vilnius"},
                {"luxembourg", "luxembourg"},
                {"macedonia", "skopje"},
                {"madagascar", "antananarivo"},
                {"malawi", "lilongwe"},
                {"malaysia", "lumpur"},
                {"maldives", "male"},
                {"mali", "bamako"},
                {"malta", "valletta"},
                {"mauritania", "nouakchott"},
                {"mexico", "mexico city"},
                {"moldova", "chisinau"},
                {"monaco", "monaco"},
                {"mongolia", "ulaanbaatar"},
                {"montenegro", "podgorica"},
                {"morocco", "rabat"},
                {"mozambique", "maputo"},
                {"namibia", "windhoek"},
                {"nepal", "kathmandu"},
                {"netherlands", "amsterdam"},
                {"zealand", "wellington"},
                {"nicaragua", "managua"},
                {"niger", "niamey"},
                {"nigeria", "abuja"},
                {"korea", "pyongyang"},
                {"ireland", "belfast"},
                {"norway", "oslo"},
                {"oman", "muscat"},
                {"pakistan", "islamabad"},
                {"palau", "melekeok"},
                {"paraguay", "asuncion"},
                {"peru", "lima"},
                {"philippines", "manila"},
                {"poland", "warsaw"},
                {"portugal", "lisbon"},
                {"qatar", "doha"},
                {"romania", "bucharest"},
                {"russia", "moscow"},
                {"rwanda", "kigali"},
                {"samoa", "apia"},
                {"arabia", "riyadh"},
                {"scotland", "edinburgh"},
                {"senegal", "dakar"},
                {"serbia", "belgrade"},
                {"seychelles", "victoria"},
                {"singapore", "singapore"},
                {"slovakia", "bratislava"},
                {"slovenia", "ljubljana"},
                {"africa", "pretoria"},
                {"sudan", "juba"},
                {"spain", "madrid"},
                {"sri-lanka", "colombo"},
                {"suriname", "paramaribo"},
                {"swaziland", "mbabana"},
                {"sweden", "stockholm"},
                {"switzerland", "bern"},
                {"syria", "damascus"},
                {"taiwan", "taipei"},
                {"tajikistan", "dushanbe"},
                {"tanzania", "dodoma"},
                {"thailand", "bangkok"},
                {"togo", "lome"},
                {"tunisia", "tunis"},
                {"turkey", "ankara"},
                {"turkmenistan", "ashgabat"},
                {"tuvalu", "funafuti"},
                {"uganda", "kampala"},
                {"ukraine", "kiev"},
                {"uk", "london"},
                {"usa", "washington"},
                {"uruguay", "montevideo"},
                {"uzbekistan", "tashkent"},
                {"venezuela", "caracas"},
                {"vietnam", "hanoi"},
                {"wales", "cardiff"},
                {"yemen", "sanaa"},
                {"zambia", "lusaka"}};

        for (String[] strings : tab) {
            if (word.contains(strings[1])) {
                word = strings[0];
            }
        }
        return word;
    }
}

