package VectorSpaceModel;

import DataSet.routers.Article;
import Divisions.Category;
import ConfusionMatrix.ProbablititySet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KNN {

    private List<Category> categories;
    private List<Article> unclassifiedData;
    private double r;
    private Metrics metrics;
    public List<ProbablititySet> results;

    public KNN(List<Category> categories, List<Article> unclassifiedData, double r, Metrics metrics) {
        this.categories = categories;
        this.unclassifiedData = unclassifiedData;
        this.r = r;
        this.metrics = metrics;
        this.results = new ArrayList<>();
        for(Article article : unclassifiedData) {
            System.out.println("--------------"+ article.getPlaces() +"----------");
            String winner = classify(article);
            results.add(new ProbablititySet(article.getPlaces(), winner));
        }
    }

    public String classify(Article article) {
        VectorFeature vf = new VectorFeature(article);
        vf.vectorize();
        Map<String, Integer> neighbours = new HashMap<>();
        findNearestNeighbours(vf, neighbours);
        String winner = "";
        int  maxVal = lookForHighestScore(neighbours);
        for (Map.Entry<String, Integer> entry : neighbours.entrySet()) {
            if (entry.getValue() == maxVal) {
               winner = entry.getKey();
            }
        }
        return (winner);
    }

    private int lookForHighestScore(Map<String, Integer> neighbours) {
        int maxVal = 0;
        for (Map.Entry<String, Integer> entry : neighbours.entrySet()) {
            if (entry.getValue() > maxVal) {
                maxVal = entry.getValue();
            }
            if (entry.getValue() > 0 ) {
                System.out.println(entry.getKey() + " - " + entry.getValue());
            }
        }
        return maxVal;
    }

    private void findNearestNeighbours(VectorFeature vf, Map<String, Integer> neighbours) {
        for (Category category : categories) {
            String key = category.label;
            int k = 0;
            neighbours.put(key, k);
            for (int vecNm = 0; vecNm < category.vector.size(); vecNm++) {
                double distance = metrics.calculate(category.vector.get(vecNm), vf.features);
                if (distance < r) {
                    k++;
                    neighbours.put(key, k);
                }
            }
        }
    }
}
