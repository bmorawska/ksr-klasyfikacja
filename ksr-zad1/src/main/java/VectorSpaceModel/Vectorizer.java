package VectorSpaceModel;

import DataSet.routers.Article;
import java.util.ArrayList;
import java.util.List;

public class Vectorizer {
    public List<Article> articles;
    public List<VectorFeature> vectorFeatures;

    public Vectorizer(List<Article> articles) {
        this.articles = articles;
        this.vectorFeatures = new ArrayList<>();
        for (Article article : articles) {
            VectorFeature vf = new VectorFeature(article);
            vf.vectorize();
            vectorFeatures.add(vf);
        }
    }
}
