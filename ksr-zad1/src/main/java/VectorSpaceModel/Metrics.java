package VectorSpaceModel;

public enum Metrics {
    CHEBYSHEV_DISTANCE,
    EUCLIDEAN_DISTANCE,
    MANHATTAN_DISTANCE;

    double calculate(double[] A, double[] B) {
        switch (this) {
            case EUCLIDEAN_DISTANCE:
                return (euclideanDistance(A, B));
            case CHEBYSHEV_DISTANCE:
                return (chebyshevDistance(A, B));
            case MANHATTAN_DISTANCE:
                return (manhattanDistance(A, B));
            default:
                return (-1);
        }
    }

    private double manhattanDistance(double[] A, double[] B) {
        double sum = 0;
        for (int i = 0; i < A.length; i++) {
            sum += Math.abs(A[i] - B[i]);
        }
        return (sum);
    }

    private double chebyshevDistance(double[] A, double[] B) {
        double sum = 0;
        for (int i = 0; i < A.length; i++) {
            sum += Math.max(A[i], B[i]);
        }
        return (sum);
    }

    private double euclideanDistance(double[] A, double[] B) {
        double sum = 0;
        for (int i = 0; i < A.length; i++) {
            sum += sqr(A[i] - B[i]);
        }
        return (sum);
    }

    private double sqr(double x) {
        return (x * x);
    }
}
