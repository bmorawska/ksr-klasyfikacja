package VectorSpaceModel;

import DataSet.routers.Article;

import java.util.Arrays;

public class VectorFeature {
    public Article article;
    public double[] features;

    public VectorFeature(Article article) {
        this.article = article;
        features = new double[Extractors.values().length];
    }

    public void vectorize() {
        features[0] = Extractors.TOTAL_NUMBER_OF_WORDS.calculate(article);
        features[1] = Extractors.NUMBER_OF_WORDS_LONGER_THAN_8_LETTERS.calculate(article);
        features[2] = Extractors.NUMBER_OF_WORDS_SHORTER_THAN_5_LETTERS.calculate(article);
        features[3] = Extractors.AVERAGE_WORD_LENGTH.calculate(article);
        features[4] = Extractors.NUMBER_OF_OCCURRENCES_OF_OWN_LABEL.calculate(article);
        features[5] = Extractors.NUMBER_OF_WORD_MONEY.calculate(article);
    }

    @Override
    public String toString() {
        return "VectorFeature [" +
                "article=" + article.getTitle() +
                " features=" + Arrays.toString(features) +
                '}';
    }
}
