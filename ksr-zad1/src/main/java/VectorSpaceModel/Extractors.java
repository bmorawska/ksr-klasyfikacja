package VectorSpaceModel;

import DataSet.routers.Article;

public enum Extractors {
    TOTAL_NUMBER_OF_WORDS,
    AVERAGE_WORD_LENGTH,
    NUMBER_OF_WORDS_LONGER_THAN_8_LETTERS,
    NUMBER_OF_WORDS_SHORTER_THAN_5_LETTERS,
    NUMBER_OF_OCCURRENCES_OF_OWN_LABEL,
    NUMBER_OF_WORD_MONEY;

    double calculate(Article article) {
        switch (this) {
            case TOTAL_NUMBER_OF_WORDS:
                return (calculateTotalNumberOfWords(article));
            case NUMBER_OF_WORDS_LONGER_THAN_8_LETTERS:
                return (calculateNumberOfWordsLongerThan8Letters(article));
            case NUMBER_OF_WORDS_SHORTER_THAN_5_LETTERS:
                return (calculateNumberOfWordsShorterThan5Letters(article));
            case AVERAGE_WORD_LENGTH:
                return (calculateAverageWordLength(article));
            case NUMBER_OF_OCCURRENCES_OF_OWN_LABEL:
                return (numberOfOccurrencessOfItsOwnLabel(article));
            case NUMBER_OF_WORD_MONEY:
                return (numberOfWordMoney(article));
        }
        return (-1);
    }

    private double calculateTotalNumberOfWords(Article article) {
        return (double) (article.getBodyWords().size());
    }

    private double calculateNumberOfWordsLongerThan8Letters(Article article) {
        int numberOfWords = 0;
        for (String word : article.getBodyWords()) {
            if (word.length() > 8) {
                numberOfWords++;
            }
        }
        return (double)(numberOfWords);
    }

    private double calculateNumberOfWordsShorterThan5Letters(Article article) {
        int numberOfWords = 0;
        for (String word : article.getBodyWords()) {
            if (word.length() < 5) {
                numberOfWords++;
            }
        }
        return (double)(numberOfWords);
    }

    private double calculateAverageWordLength(Article article) {
        int numberOfChars = 0;
        int numberOfWords = article.getBodyWords().size();
        for (String word : article.getBodyWords()) {
            numberOfChars += word.toCharArray().length;
        }
        return ((double)numberOfChars / (double)numberOfWords);
    }

    private double numberOfOccurrencessOfItsOwnLabel(Article article) {
        int numberOfOccurrences = 0;
        for (String word: article.getBodyWords()) {
            for (String label : article.getPlaces()) {
                if (word.contains(label)) {
                    numberOfOccurrences++;
                }
            }
        }
        return (double)(numberOfOccurrences);
    }

    private double numberOfWordMoney(Article article) {
        int numberOfWords = 0;
        for (String word: article.getBodyWords()) {
            if(word.contains("mone")) {
                numberOfWords++;
            }
        }
        return numberOfWords;
    }
}
