package KeyWords;

import DataSet.routers.Article;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class KeyWordChooser {

    public List<Article> articles;
    private double edge;

    public KeyWordChooser(List<Article> articles, double edge) {
        this.articles = articles;
        this.edge = edge;
    }

    public void chooseKeyWords() {
        List<Map<String, Integer>> dictioneries = new ArrayList<>();
        List<Map<String, Double>> TFdictioneries = new ArrayList<>();
        Map<String, Integer> allDocumentsContainsWords;
        Map<String, Double> IDFdictionary;
        List<Map<String, Double>> TDIDF;

        for (Article article : articles) {
            Map<String, Integer> dictionary = createArticleDictionary(article);
            dictioneries.add(dictionary);
            TFdictioneries.add(computeTF(dictionary));
        }

        allDocumentsContainsWords = combineDictionaries(dictioneries);
        IDFdictionary = computeIDF(allDocumentsContainsWords);
        TDIDF = computeTFIDF(TFdictioneries, IDFdictionary);

        chooseKeyWords(TDIDF);
    }

    private void chooseKeyWords(List<Map<String, Double>> dictionaries) {
        for (int i = 0; i < dictionaries.size(); i++) {
            List<String> newBodyWords = new ArrayList<>();
            for (Map.Entry<String, Double> entry : dictionaries.get(i).entrySet()) {
                if (entry.getValue() > edge) {
                    newBodyWords.add(entry.getKey());
                }
            }
            articles.get(i).setBodyWords(newBodyWords);
        }
    }

    private List<Map<String, Double>> computeTFIDF(List<Map<String, Double>> tfs, Map<String, Double> idfs) {
        for (Map<String, Double> dictionary : tfs) {
            for (Map.Entry<String, Double> tfValue : dictionary.entrySet()) {
                double tfidf = tfValue.getValue() * idfs.get(tfValue.getKey());
                dictionary.put(tfValue.getKey(), tfidf);
            }
        }
        return (tfs);
    }

    private Map<String, Double> computeIDF(Map<String, Integer> allDocumentsContainsWords) {
        Map<String, Double> idfTable = new HashMap<>(allDocumentsContainsWords.values().size());
        allDocumentsContainsWords.forEach((s, d) -> idfTable.put(s, (double)d));
        for (Map.Entry<String, Double> entry : idfTable.entrySet()) {
            double tdfValue = Math.log(articles.size() / entry.getValue());
            idfTable.put(entry.getKey(), tdfValue);
        }
        return (idfTable);

    }

    private Map<String, Integer> combineDictionaries(List<Map<String, Integer>> dictioneries) {
        Map<String, Integer> articleDictionary = new HashMap<>();
        for (Article article : articles) {
            for (String word : article.getBodyWords()) {
                articleDictionary.put(word, 0);
            }
        }
        for (Map.Entry<String, Integer> wordDic : articleDictionary.entrySet()) {
            for (Article article : articles) {
                for (String word : article.getBodyWords()) {
                    if (wordDic.getKey().equals(word)) {
                        articleDictionary.put(wordDic.getKey(), wordDic.getValue() + 1);
                        break;
                    }
                }
            }
        }
        return (articleDictionary);
    }

    private Map<String, Double> computeTF(Map<String, Integer> dictionary) {
        Map<String, Double> tfTable = new HashMap<>(dictionary.values().size());
        dictionary.forEach((s, d) -> tfTable.put(s, (double)d));
        for (Map.Entry<String, Double> entry : tfTable.entrySet()) {
            double tfValue = entry.getValue() / dictionary.values().size();
            tfTable.put(entry.getKey(), tfValue);
        }
        return (tfTable);
    }


    private Map<String, Integer> createArticleDictionary(Article article) {
        Map<String, Integer> articleDictionary = new HashMap<>();
        int numberOfOccurrences = 0;
        for (String word : article.getBodyWords()) {
            if (articleDictionary.containsKey(word)) {
                numberOfOccurrences = articleDictionary.get(word);
                articleDictionary.put(word, (numberOfOccurrences + 1));
            } else {
                articleDictionary.put(word, 1);
            }
        }
        return (articleDictionary);
    }
}
