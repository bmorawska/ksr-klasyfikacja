package KSR;

import ConfusionMatrix.MatricesCreator;
import DataSet.routers.ReadData;
import Divisions.Category;
import Divisions.OnClassDivider;
import KeyWords.KeyWordChooser;
import PreprocessingData.Tokenizer;
import Divisions.LearnTestDivider;
import VectorSpaceModel.KNN;
import VectorSpaceModel.Metrics;
import VectorSpaceModel.Vectorizer;

public class App {
    public static void main(String[] args) {
        /* STAŁE */
        final String directoryName = "json-reuters-21578/json-data";
        final double edge = 0.0;
        final int testSetPercent = 40;
        final String label = "places";

        /* TYLKO DO TESTÓW!!!*/
        final String testdirectoryName = "test-json-reuters-21578";
        final String testShort = "test-short";

        /* WCZYTYWANIE DANYCH */
        ReadData readData = new ReadData(testdirectoryName);
        readData.readDirectory();

        /* WSTĘPNE PRZETWARZANIE DANYCH */
        Tokenizer tokenizer = new Tokenizer(readData.articles);
        tokenizer.tokenize();

        /* PODZIAŁ NA ZBIÓR TESTOWY I UCZĄCY*/
        LearnTestDivider learnTestDivider = new LearnTestDivider(tokenizer.articles, testSetPercent);

        /* WYBÓR SŁÓW KLUCZOWYCH */
        KeyWordChooser keyWordChooser = new KeyWordChooser(learnTestDivider.learningSetArticles, edge);
        keyWordChooser.chooseKeyWords();

        /* POLICZENIE WARTOŚCI CECH */
        Vectorizer vectorizer = new Vectorizer(keyWordChooser.articles);
        OnClassDivider onClassDivider = new OnClassDivider(vectorizer.vectorFeatures, "places");

        for (Category category : onClassDivider.categories) {
            System.out.println(category);
        }

        /* KNN */
        KNN knn = new KNN(onClassDivider.categories, learnTestDivider.testSetArticles, 10.0, Metrics.EUCLIDEAN_DISTANCE);

        /* OCENA JAKOŚCI */
        MatricesCreator matricesCreator = new MatricesCreator(knn.results, onClassDivider.categories);
    }
}

