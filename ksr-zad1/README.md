
##Dane do zadania
###Źródło danych
Dane pochodzą z [repozytorium mihaibogdan10/json-reuters-21578](https://github.com/mihaibogdan10/json-reuters-21578). 
Są to podane do zadania dane ``reuters-21578`` (które pierwotnie były w formacie SGML), tyle że w formacie JSON.
Zmiana formatu wynikała z prostszego wczytania ich za pomocą gotowego komponentu.

###Zmiany w plikach źródłowych
Na początku każdego pliku zostało dodane:

```
{ 
    "articles" :   
        [  
            ...To co jest teraz...
        ]
}
```
, w celu poprawnego wczytania do tablicy w programie.

###Wczytywanie
Dane wczytywane są za pomocą parsera gson. Do działania programu potrzebny jest również pakiet ``commons-lang``.


``` 
<dependency>
 <groupId>com.google.code.gson</groupId>
 <artifactId>gson</artifactId>
 <version>2.6.2</version>
</dependency>
<!-- https://mvnrepository.com/artifact/commons-lang/commons-lang -->
<dependency>
 <groupId>commons-lang</groupId>
 <artifactId>commons-lang</artifactId>
 <version>2.2</version>
</dependency>
```

Klasy zostały utworzone przy pomocy [generatora klas, na podstawie plików JSON](http://www.jsonschema2pojo.org/).


##Wstępne przerabianie danych

Wstępne przerabianie danych bazuje na książce:
```
Clojure for Data Science
Statistics, big data, and machine learning for
Clojure programmers
Henry Garner
```

oraz wiedzy pozyskanej na zajęciach z Komputerowych Systemów Rozpoznawania.

Proces ten polegał na przerobieniu tekstu ``Body`` w każdym z artykułów znajdujących się w plikach
w taki sposób, żeby w jak największym stopniu zwiększyć powtrzalność tych samych wyrazów (tylko 
odmienionych/zapisanych w innej formie) w każdym artykule. Do procesu przetwarzania należało między innymi:
1. Usunięcie znaków innych niż litery alfabetu.
2. Zamiana wszystkich wielkich liter na małe.
3. Zamiana czasowników nieregularnych na ich formę w bezokoliczniku.
4. Użycie [PorterStemmer](http://people.scs.carleton.ca/~armyunis/projects/KAPI/porter.pdf) 
(źródło: [tutaj](https://tartarus.org/martin/PorterStemmer/))
5. Usunięcie wyrazów o długości 2 i mniejszych.





